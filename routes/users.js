var express = require('express');
var router = express.Router();

router.get('/login', function(req, res, next) {
  res.render('login.html');
});

router.get('/register', function(req, res, next) {
  res.render('register.html');
});

/* GET users listing. */
router.get('/:id', function(req, res, next) {
  res.render('user.html', {id: req.params.id});
});

module.exports = router;
