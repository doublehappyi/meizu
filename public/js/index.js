(function($){
    $(function(){
        var $ctrl = $('.slider-ctrl');
        var bs = new BestSlider(".slider", {
            auto:true,
            keyboard:true,
            animation:'fade',
            speed:600,
            interval:4000,
            before:function(bs, idx){
                $ctrl.filter('.slider-ctrl-active').removeClass('slider-ctrl-active');
                $ctrl.eq(idx).addClass('slider-ctrl-active');
            }
        }); 
        
        $ctrl.click(function(){
            var idx = $ctrl.index(this);
            bs.slideTo(idx);
        });
    });
})(jQuery);