for i in $(lsof -i:8080 | awk '{print $2}' | egrep '[0-9]+')
do
    kill -9 $i
done