var gulp = require('gulp');

var jshint = require('gulp-jshint');
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var less = require('gulp-less');
var minify = require('gulp-minify-css');
var watch = require('gulp-watch');
//var sourcemaps = require('gulp-sourcemaps');
var prefixer = require('gulp-autoprefixer');

var path = {
    lib:{
        js:['public/js/lib/jquery.js', 'public/js/lib/jquery.easing.js', 'public/js/lib/jquery.mousewheel.js', 'public/js/lib/Best.js'],
        less:['public/less/base/normalize.css','public/less/base/base.less','public/less/base/header.less','public/less/base/footer.less']
    },
    index:{
        js:['public/js/BestSlider/BestSlider.js','public/js/index.js'],
        less:['public/js/BestSlider/BestSlider.css','public/less/index.less']
    },
    store:{
        index:{
            js:['public/js/BestSlider/BestSlider.js','public/js/store/index.js'],
            less:['public/js/BestSlider/BestSlider.css','public/less/store/index.less']
        }
    }
}
gulp.task('lib', function(){
    gulp.src(path.lib.js).pipe(concat('lib.js')).pipe(gulp.dest('build/js'));
    gulp.src(path.lib.less).pipe(less()).pipe(concat('lib.css'))
    .pipe(prefixer({
        browsers:['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
    }))
    .pipe(gulp.dest('build/css'));
});
gulp.task('index', function(){
    gulp.src(path.index.js).pipe(concat('index.js')).pipe(gulp.dest('build/js'));
    gulp.src(path.index.less).pipe(less()).pipe(concat('index.css'))
    .pipe(prefixer({
        browsers:['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
    }))
    .pipe(gulp.dest('build/css'));
});
gulp.task('store', function(){
    gulp.src(path.store.index.js).pipe(concat('index.js')).pipe(gulp.dest('build/js/store'));
    gulp.src(path.store.index.less).pipe(less()).pipe(concat('index.css'))
    .pipe(prefixer({
        browsers:['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
    }))
    .pipe(gulp.dest('build/css/store'));
});
//watch后面参数是执行watch之前要执行的任务
//watch()里面的参数：要watch的文件,对应的文件需要执行的任务
gulp.task('watch',['lib', 'index', 'store'], function(){
    // gulp.watch([path.lib.js, path.lib.less, path.index.js,
    // path.index.less, path.store.index.js, path.store.index.less], 
    // ['lib', 'index', 'store']); 
    gulp.watch(['public/js/**/*.js', 'public/less/**/*.less'], 
    ['lib', 'index', 'store']);
});
// gulp.task('build', ['lib', 'index', 'store'], function(){
       
// });